# React Tic Tac Toe Game

This project is a simple Tic Tac Toe game implemented using React. It features four basic components: GameBoard, GameOver, Log, and Player. The goal of the project is to demonstrate various React practices, including building reusable components, working with state, user input and two-way binding, rendering multi-dimensional lists, lifting state up, deriving state from props, and sharing state across components.

## Features

- **GameBoard:** The main game board where players can make their moves.
- **GameOver:** Displays the game result (win, lose, or draw) in a popup once the game is over and provide option for resetting the game.
- **Log:** Keeps track of the moves made by both players during the game and list it below the game board.
- **Player:** Allows players to set and edit their names and displays whose turn it is with their corresponding symbols.

## Getting Started

To run the application locally, follow these steps:

1. Clone the repository to your local machine:

git clone https://gitlab.com/drazhev/tictactoe.git

3. Install dependencies using npm:

npm install

4. Run the application:

npm run dev

5. Open your browser and navigate to `http://localhost:3000` to play the Tic Tac Toe game.
